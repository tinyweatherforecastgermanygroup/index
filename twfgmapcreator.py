"""

@title TinyWeatherForecastGermany - leaflet map featuring data provided by DWD/WMO
 - GitLab Pages page

@author Jean-Luc Tibaux (https://gitlab.com/eUgEntOptIc44)

@license GPLv3

@since July 2021

No warranty of any kind provided. Use only at your own risk only.
Not meant to be used in commercial or
 in general critical/productive environments at all.

Copyright of the remixed public data:
DWD, WMO, OpenStreetMap, OpenTopoMap, Stamen

"""

# from datetime import datetime, date, timedelta
import json
import logging
import sys
from pathlib import Path

# from pprint import pprint
import folium
import folium.plugins

# timezone UTC -> docs: https://dateutil.readthedocs.io/en/stable/tz.html#dateutil.tz.tzutc
# from dateutil.tz import tzutc
import htmlmin
import humanize  # human readable file sizes, timestamps, ...
import regex
import requests
from bs4 import BeautifulSoup
from cssmin import cssmin
from jsmin import jsmin
from webassets import Bundle, Environment

working_dir = Path("public")
working_dir.mkdir(parents=True, exist_ok=True)  # create directory if not exists

dwd_ows = "https://maps.dwd.de/geoserver/ows?"
dwd_img_format = "image/png"
bs4_features = "html.parser"

log_p = working_dir / "debug.log"
try:
    logging.basicConfig(
        format="%(asctime)-s %(levelname)s [%(name)s]: %(message)s",
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(str(log_p.absolute()), encoding="utf-8"),
            logging.StreamHandler(),
        ],
    )
except Exception as error_msg:
    logging.error(f"while logger init! -> error: {error_msg}")

folium_map = folium.Map(
    location=[50.9, 10.3],
    tiles="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    name="OpenStreetMap (OSM)",
    zoom_start=6,
    control_scale=True,
    attr="© Warngebiete, WMS-Layers & Stations -> "
    '<a href="https://opendata.dwd.de/">DWD</a>/'
    '<a href="https://www.wwis.dwd.de/de/home.html">WMO</a> | '
    '<a href="https://leafletjs.com/">Leaflet</a> | Map Data by'
    ' © <a href="http://openstreetmap.org/">OpenStreetMap</a>'
    ", under"
    ' <a href="http://www.openstreetmap.org/copyright">ODbL</a>.',
)

folium.TileLayer(
    tiles="https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png",
    name="OpenTopoMap",
    attr="opentopomap",
).add_to(folium_map)
logging.debug("added 'opentopomap' tile layer ")

#region stations -> DWD/WMO weather stations

stations_json_file = working_dir / "twfg-stations.json"
stations_json_size = stations_json_file.stat().st_size

fatal_err_msg = "FATAL ERROR script execution aborted!"

if stations_json_file.exists() is not True:
    logging.error(
        f" 'stations' json file '{stations_json_file.absolute()}'"
        " does not exist or is not readable!"
    )
    sys.exit(fatal_err_msg)
elif stations_json_size < 1:
    logging.error(
        f" size of 'stations' json file '{stations_json_file.absolute()}'"
        f" -> '{stations_json_size}' is too small!"
    )
    sys.exit(fatal_err_msg)
else:
    logging.debug(
        "successfully loaded 'stations' json"
        f" file '{stations_json_file.absolute()}'"
        f" ({humanize.naturalsize(stations_json_size, binary=True)}) "
    )

try:
    with open(stations_json_file, "r", encoding="utf-8") as file_handle:
        stations_json_list = json.loads(str(file_handle.read()))
        logging.debug(f"parsed {len(stations_json_list)} stations")
except Exception as error_msg:
    logging.error(f"failed to open '{stations_json_file.absolute()}' -> error: {error_msg}")
    sys.exit(fatal_err_msg)

# docs see: https://python-visualization.github.io/folium/plugins.html#folium.plugins.MarkerCluster
station_cls = folium.plugins.MarkerCluster(name="DWD/WMO stations").add_to(
    folium_map
)

for station_index in range(len(stations_json_list)):
    station_tmp = stations_json_list[station_index]
    folium.CircleMarker(
        location=[station_tmp["latitude"], station_tmp["longitude"]],
        radius=6,
        popup=folium.Popup(
            html=f"<b>{station_tmp['name']}.</b>"
            f" <br>{station_tmp['id']}<br>"
            f"{station_tmp['altitude']}m ",
            parse_html=False,
        ),
        tooltip=str(station_tmp["name"]),
        color="blue",
        fill=False,
    ).add_to(station_cls)

logging.debug("completed iteration of stations")

#endregion

#region Warngebiete -> warning areas by DWD

warngebiete_names = [
    "dwd:Warngebiete_Binnenseen",
    "dwd:Warngebiete_Bundeslaender",
    "dwd:Warngebiete_Gemeinden",
    "dwd:Warngebiete_Kreise",
    "dwd:Warngebiete_Kueste"  # ,
    # "dwd:Warngebiete_See", # does apparently not exists anymore (July 2023)
]

for wg_name_tmp in warngebiete_names:
    try:
        warngebiete_raw_tmp = requests.get(
            "https://maps.dwd.de/geoserver/dwd/ows"
            "?service=WFS&version=1.0.0&request=GetFeature"
            f"&typeName={wg_name_tmp}&maxFeatures=20000"
            "&OutputFormat=application/json"
        )

        warngebiete_raw_tmp = str(warngebiete_raw_tmp.text).strip()

        try:
            warngebiete_json_tmp = json.loads(warngebiete_raw_tmp)
            logging.debug(
                f"retrieved {len(warngebiete_json_tmp['features'])}"
                f" geojson features for warngebiet '{wg_name_tmp}' "
            )
        except Exception as error_msg:
            logging.error(
                "parsing of json data for warngebiete"
                f" '{wg_name_tmp}' failed -> error: {error_msg}"
            )

        warngebiet_slug = regex.sub(r"(?im)[^A-z\d]+", "_", str(wg_name_tmp))
        warngebiet_file_name = (
            f"twfg-warngebiete-{warngebiet_slug.lower()}-geojson-temp.json"
        )

        warngebiet_p = working_dir / Path(warngebiet_file_name)
        with open(warngebiet_p, "w+", encoding="utf-8") as file_handle:
            file_handle.write(warngebiete_raw_tmp)

    except Exception as error_msg:
        logging.error(
            f"requesting of data for warngebiete '{wg_name_tmp}'"
            f" failed -> error: {error_msg}"
        )

geojson_files = list(working_dir.glob("twfg-warngebiete-*geojson-*.json"))
# pprint(geojsonFiles)

geojson_colors = {  # colors for geojson files retrieved from DWD (see above)
    # expect ca. 23 geojson features
    "twfg-warngebiete-dwd_warngebiete_binnenseen-geojson-temp.json": {
        "name": "Binnenseen",
        "stroke": "#037CFF",
        "fill": "#037CFF",
    },
    # expect 16 geojson features
    "twfg-warngebiete-dwd_warngebiete_bundeslaender-geojson-temp.json": {
        "name": "Bundesländer",
        "stroke": "#656565",
        "fill": "#656565",
    },
    # expect ca. 11.176 geojson features
    "twfg-warngebiete-dwd_warngebiete_gemeinden-geojson-temp.json": {
        "name": "Gemeinden",
        "stroke": "rgb(249 134 23 / 73%)",  # "stroke":"rgb(249 134 23 / 73%)",
        "fill": "rgb(249 134 23 / 73%)",
    },
    # expect ca. 402 geojson features
    "twfg-warngebiete-dwd_warngebiete_kreise-geojson-temp.json": {
        "name": "Kreise",
        # "stroke":"rgb(214 112 214 / 86%)",
        "stroke": "rgb(214 112 214 / 76%)",
        "fill": "rgb(214 112 214 / 76%)",
    },
    # expect ca. 8 geojson features
    "twfg-warngebiete-dwd_warngebiete_kueste-geojson-temp.json": {
        "name": "Küste",
        "stroke": "rgb(53 195 245 / 84%)",  # "stroke":"rgb(53 195 245 / 84%)",
        "fill": "rgb(53 195 245 / 84%)",
    },
    # expect ca. 20 geojson features
    "twfg-warngebiete-dwd_warngebiete_see-geojson-temp.json": {
        "name": "See",
        "stroke": "rgb(30 124 255 / 84%)",  # "stroke":"rgb(30 124 255 / 84%)",
        "fill": "rgb(30 124 255 / 84%)",
    },
}

for geojson_file_tmp in geojson_files:
    # iterate through file names and load data from disk
    # instead of memory (->e.g. from a list variable filled above)
    # to reduce memory load

    try:
        # pprint(geojson_colors[str(geojson_file_tmp.name)])
        geojson_file_name_tmp = str(geojson_file_tmp.name).strip()

        fill_color_tmp = str(geojson_colors[geojson_file_name_tmp]["fill"])
        stroke_color_tmp = str(geojson_colors[geojson_file_name_tmp]["stroke"])

        try:
            data_tmp = str(
                open(str(geojson_file_tmp.absolute()), encoding="utf-8").read()
            )
        except Exception as error_msg:
            data_tmp = ""
            logging.error(
                f"failed to open geojson file "
                f"'{geojson_file_tmp.absolute()}'"
                f" -> error: {error_msg}"
            )

        if data_tmp != "":
            # if False layer is hidden
            #  -> can be toggled by user from top-right 'layer' control in UI
            show_layer_on_start = False

            # we had to limit this to reduce memory load
            if (
                "see" in geojson_file_name_tmp.lower()
                or "laender" in geojson_file_name_tmp.lower()
            ):
                show_layer_on_start = True

            folium_map.add_child(
                folium.GeoJson(
                    data=data_tmp,
                    style_function=lambda x, fillColorTemp=fill_color_tmp, strokeColorTemp=stroke_color_tmp: {
                        "fillColor": fillColorTemp,
                        "fillOpacity": "0.1",
                        "color": strokeColorTemp,
                        "strokeWidth": "0.75",
                        "strokeOpacity": "0.5",
                    },
                    name=geojson_colors[str(geojson_file_tmp.name)]["name"],
                    popup=folium.GeoJsonPopup(
                        ["NAME", "WARNCELLID", "WARNCENTER", "MIN_HEIGHT", "MAX_HEIGHT"]
                    ),
                    tooltip=folium.GeoJsonTooltip(fields=["NAME"], labels=False),
                    zoom_on_click=True,
                    smooth_factor=1.1,
                    embed=True,
                    show=show_layer_on_start,
                )
            )
            logging.debug(f"added map layer '{geojson_file_name_tmp}'")
        else:
            logging.error(f"failed to add map layer '{geojson_file_name_tmp}'")
    except Exception as error_msg:
        logging.error(
            f"failed to add map layer '{geojson_file_name_tmp}' -> error: {error_msg}"
        )

#endregion

#region DWD WMS Map Layers
#  -> folium docs: https://python-visualization.github.io/folium/modules.html#folium.raster_layers.WmsTileLayer
#  -> inspired by: https://github.com/guidocioni/world-weather-map/blob/master/webapp.py
folium.WmsTileLayer(
    url=dwd_ows,
    name="DWD Sat EU (RGB) (3h)",
    layers="dwd:Satellite_meteosat_1km_euat_rgb_day_hrv_and_night_ir108_3h",
    fmt=dwd_img_format,
    show=False,
    transparent=True,
    opacity=0.7,
    version="1.3.0",
    detectRetina=True,
).add_to(folium_map)

logging.debug("added 'dwd:Satellite_meteosat_1km_euat_rgb_day_hrv_and_night_ir108_3h' WMS tile layer ")

folium.WmsTileLayer(
    url=dwd_ows,
    name="DWD WN radar",
    layers="dwd:WN-Produkt",
    fmt=dwd_img_format,
    show=True,
    transparent=True,
    opacity=0.6,
    version="1.3.0",
    styles="wn-produkt-ohne-abdeckung",
    detectRetina=True,
).add_to(folium_map)

logging.debug("added 'dwd:WN-Produkt' WMS tile layer ")

folium.WmsTileLayer(
    url=dwd_ows,
    name="DWD Warnungen Bundesländer",
    layers="dwd:Warnungen_Bundeslaender",
    fmt=dwd_img_format,
    show=False,
    transparent=True,
    opacity=0.5,
    version="1.3.0",
    # styles='',
    detectRetina=True,
).add_to(folium_map)

logging.debug("added 'dwd:Warnungen_Bundeslaender' WMS tile layer ")

folium.WmsTileLayer(
    url=dwd_ows,
    name="DWD Warnungen Gemeinden",
    layers="dwd:Warnungen_Gemeinden",
    fmt=dwd_img_format,
    show=False,
    transparent=True,
    opacity=0.5,
    version="1.3.0",
    # styles='',
    detectRetina=True,
).add_to(folium_map)

logging.debug("added 'dwd:Warnungen_Gemeinden' WMS tile layer ")

folium.WmsTileLayer(
    url=dwd_ows,
    name="DWD Warnungen Landkreise",
    layers="dwd:Warnungen_Landkreise",
    fmt=dwd_img_format,
    show=False,
    transparent=True,
    opacity=0.5,
    version="1.3.0",
    # styles='',
    detectRetina=True,
).add_to(folium_map)

logging.debug("added 'dwd:Warnungen_Landkreise' WMS tile layer")

folium.WmsTileLayer(
    url=dwd_ows,
    name="DWD Warnungen Küste",
    layers="dwd:Warnungen_Kueste",
    fmt=dwd_img_format,
    show=False,
    transparent=True,
    opacity=0.5,
    version="1.3.0",
    # styles='',
    detectRetina=True,
).add_to(folium_map)

logging.debug("added 'dwd:Warnungen_Kueste' WMS tile layer")

folium.WmsTileLayer(
    url=dwd_ows,
    name="DWD Warnungen Binnenseen",
    layers="dwd:Warnungen_Binnenseen",
    fmt=dwd_img_format,
    show=False,
    transparent=True,
    opacity=0.5,
    version="1.3.0",
    # styles='',
    detectRetina=True,
).add_to(folium_map)

logging.debug("added 'dwd:Warnungen_Binnenseen' WMS tile layer")

folium.WmsTileLayer(
    url=dwd_ows,
    name="DWD UV Dosis Global CL",
    layers="dwd:UV_Dosis_Global_CL",
    fmt=dwd_img_format,
    show=False,
    transparent=True,
    opacity=0.5,
    version="1.3.0",
    # styles='',
    detectRetina=True,
).add_to(folium_map)

logging.debug("added 'dwd:UV_Dosis_Global_CL' WMS tile layer")

folium.WmsTileLayer(
    url=dwd_ows,
    name="DWD UV Dosis EU CL",
    layers="dwd:UV_Dosis_EU_CS",
    fmt=dwd_img_format,
    show=False,
    transparent=True,
    opacity=0.5,
    version="1.3.0",
    # styles='',
    detectRetina=True,
).add_to(folium_map)

logging.debug("added 'dwd:UV_Dosis_EU_CS' WMS tile layer")

#endregion DWD WMS Map Layers

folium.LayerControl().add_to(folium_map)

#region folium plugins

# folium.plugins.Terminator().add_to(foliumMap) # adds day/night overlay
# foliumMap.add_child(folium.plugins.Draw(export=True, filename='drawing.geojson', position='topleft',
#                                         draw_options=None, edit_options=None))
# draw and modify geojson on map # docs: http://leaflet.github.io/Leaflet.draw/docs/leaflet-draw-latest.html

folium_map.add_child(folium.plugins.LocateControl())

# adds MiniMap to map -> source: https://github.com/Norkart/Leaflet-MiniMap
folium_map.add_child(
    folium.plugins.MiniMap(toggle_display=True, auto_toggle_display=True)
)

folium_map.add_child(folium.plugins.MeasureControl())

# plugin showing mouse coordinates -> docs see: https://python-visualization.github.io/folium/plugins.html#folium.plugins.MousePosition
fmtr = "function(num) {return L.Util.formatNum(num, 3) + ' º ';};"
folium.plugins.MousePosition(
    position="topright",
    separator=" | ",
    prefix="",
    empty_string="",
    lat_formatter=fmtr,
    # German labels: prefix -> "Koordinaten des Cursors:"
    # empty_string -> "Keine Position verfügbar."
    lng_formatter=fmtr,
).add_to(folium_map)

# docs see: https://python-visualization.github.io/folium/plugins.html#folium.plugins.Fullscreen
folium.plugins.Fullscreen(
    position="topleft",
    title="Vollbild",
    title_cancel="Vollbild beenden",
    force_separate_button=False,
).add_to(folium_map)

#endregion

logging.debug("rendering folium map ... ")

# parse html to modify elements -> rendering map to html string
#  -> source: https://github.com/python-visualization/folium/issues/781
map_soup = BeautifulSoup(str(folium_map.get_root().render()), features=bs4_features)

# with open(str(Path(working_dir / "map.html").absolute()), "w+", encoding="utf-8") as fh:
#         fh.write(str(map_soup))
# sys.exit(0)

logging.debug("parsed folium map html data ")

script_tags = map_soup.select("head script")

script_src_list = []
for script_tag_tmp in script_tags:
    src_tmp = str(script_tag_tmp.get("src")).strip().replace("None", "")
    if len(src_tmp) > 4:
        script_src_list.append(src_tmp)
        script_tag_tmp.decompose()

script_src_list.append("js/dwd_crowd.js")

#region AQI data by EEA
try:
    headers = {
        "User-Agent": "",
        "DNT": "1",
        "Origin": "https://airindex.eea.europa.eu",
        "Referer": "https://airindex.eea.europa.eu/",
        "Accept-Language": "en-US,en;q=0.5",
    }

    api_base_url = (
        "https://dis2datalake.blob.core.windows.net/airquality-derivated/AQI/"
    )

    index_req = requests.get(f"{api_base_url}content/index.json", headers=headers)
    # -> e.g. Mon, 02 May 2022 10:20:08 GMT
    logging.debug(
        f"AQI station data -> Last-Modified: {index_req.headers['Last-Modified']}"
    )

    index_json = index_req.json()
    # pprint(index_json)

    stations_file = str(index_json["contents"][0])

    stations_req = requests.get(
        f"{api_base_url}content/{stations_file}", headers=headers
    )
    stations_file = working_dir / "AQI-stations.json"

    stations_json = stations_req.json()
    # pprint(stations_json)

    # with open(stations_file, 'r', encoding='utf-8') as fh:
    #     stations_json = json.loads(str(fh.read()))

    len_stations = len(stations_json)

    logging.debug(f"received {len_stations} AQI stations from datalake")

    if len_stations > 0:
        with open(stations_file, "w", encoding="utf-8") as file_handle:
            file_handle.write(json.dumps(stations_json, indent=4))

        # insert javascript for frontend
        script_src_list.append("js/aqi_stations.js")

        logging.debug("succesfully inserted js for AQI stations")
    else:
        logging.debug("failed to insert AQI stations -> length of stations is invalid")

except Exception as error_msg:
    logging.debug(f"processing of AQI stations failed -> error: {error_msg}")
#endregion

# pprint(script_src_list)

style_tags = map_soup.select('head link[rel="stylesheet"]')

style_href_list = []
for style_tag_tmp in style_tags:
    src_tmp = str(style_tag_tmp.get("href")).strip().replace("None", "")
    if len(src_tmp) > 4:
        style_href_list.append(src_tmp)
        style_tag_tmp.decompose()

# pprint(style_href_list)

oldjs = list(Path(working_dir / "js").glob("script_map_*.js"))
# pprint(oldjs)

for oldjs_tmp in oldjs:
    try:
        oldjs_tmp.unlink()
    except Exception as error_msg:
        logging.error(f"failed to delete '{oldjs_tmp.absolute()}' -> error: {error_msg}")

oldcss = list(Path(working_dir / "css").glob("style_map_*.css"))
# pprint(oldcss)

for oldcss_tmp in oldcss:
    try:
        oldcss_tmp.unlink()
    except Exception as error_msg:
        logging.error(f"failed to delete '{oldcss_tmp.absolute()}' -> error: {error_msg}")

assets_env = Environment(
    directory=str(working_dir.absolute()),
    url="https://tinyweatherforecastgermanygroup.gitlab.io/index",
)  # , url="/")

js = Bundle(
    *tuple(script_src_list), filters="rjsmin", output="js/script_map_%(version)s.min.js"
)
assets_env.register("js_map", js)

css = Bundle(
    *tuple(style_href_list),
    filters="cssmin",
    output="css/style_map_%(version)s.min.css",
)
assets_env.register("css_map", css)

assets_html_str = ""

try:
    js_urls = list(assets_env["js_map"].urls())
    for js_url in js_urls:
        assets_html_str += f'<script src="{js_url}"></script>\n'
except Exception as error_msg:
    logging.error(f"failed to minify js assets -> error: {error_msg}")

try:
    css_urls = list(assets_env["css_map"].urls())
    for css_url in css_urls:
        css_path = Path("css") / Path(
            css_url.replace(str(assets_env.url), "")
            .replace("\\", "")
            .replace("/", "")
            .replace("css", "")
            + "css"
        )

        with open(str(working_dir / css_path), "r", encoding="utf-8") as file_handle:
            css_contents_tmp = str(file_handle.read())

        css_contents_tmp = regex.sub(
            regex.compile(
                r"(?im)(\.\.\/fonts\/fontawesome\-webfont\.)([a-z\d]+)(\?[^\'\"]+)"
            ),
            r"\g<1>\g<2>",
            css_contents_tmp,
        )

        with open(str(working_dir / css_path), "w+", encoding="utf-8") as file_handle:
            file_handle.write(str(css_contents_tmp))

        assets_html_str += f'<link rel="stylesheet" href="{css_url}" media="all">\n'
except Exception as error_msg:
    logging.error(f"failed to minify css assets -> error: {error_msg}")

try:
    # parse html to modify elements
    map_soup.select("meta")[0].insert_after(
        BeautifulSoup(str(assets_html_str), features=bs4_features)
    )
except Exception as error_msg:
    logging.error(
        f"failed to insert js and css assets into html rendering result -> error: {error_msg}"
    )

try:
    if len(map_soup.select("head title")) > 0:
        map_soup.select("head title")[0].string = "map - Tiny Weather Forecast Germany"
    else:
        # parse html to modify elements
        map_soup.select("meta")[0].insert_after(
            BeautifulSoup(
                str("<title>map - Tiny Weather Forecast Germany</title>"),
                features=bs4_features,
            )
        )
except Exception as error_msg:
    logging.error(f"failed to set head -> target: 'head title' -> error: {error_msg}")

try:
    meta_tags = list(map_soup.select("head meta"))
    if len(meta_tags) > 0:
        for tag in meta_tags:
            try:
                if tag.content is not None:
                    meta_tags.content = regex.sub(
                        r"(?im)[\n\r\t]+", " ", str(tag.string)
                    )
            except Exception as error_msg:
                logging.error(
                    f"failed to clean meta tag -> target: '{tag}' -> error: {error_msg}"
                )
except Exception as error_msg:
    logging.error(f"failed to clean meta tags -> target: 'head meta' -> error: {error_msg}")

try:
    inline_js = list(map_soup.select("script"))
    for tag in inline_js:
        try:
            if tag.string is not None:
                tag.string = jsmin(str(tag.string))
        except Exception as error_msg:
            logging.error(f"failed to minify in-line js tag -> error: {error_msg}")

except Exception as error_msg:
    logging.error(f"failed to minify in-line js -> target: 'script' -> error: {error_msg}")

try:
    inline_css = list(map_soup.select("style"))
    for tag in inline_css:
        try:
            if tag.string is not None:
                tag.string = cssmin(str(tag.string))

                tag.string = regex.sub(
                    r"(?m)[\.\#]*\w+{}", "", str(tag.string)
                )  # remove empty tags
        except Exception as error_msg:
            logging.error(f"failed to minify in-line css tag -> error: {error_msg}")

except Exception as error_msg:
    logging.error(f"failed to minify in-line css -> target: 'style' -> error: {error_msg}")

map_soup = str(map_soup)

map_soup = regex.sub(
    r"(?m)(base_layers[^\:]*\:[\{]*\{)(\")(https\:\/\/\{s\}\.tile\.openstreetmap\.org\/\{z\}\/\{x\}\/\{y\}\.png)(\")",
    r'\g<1>"OpenStreetMap"',
    str(map_soup),
)

map_soup = regex.sub(r"(?m)(map\_)([A-z\d]{32})", "twfg_folium_map", str(map_soup))

map_html_file = working_dir / "map.html"
try:
    with open(map_html_file, "w+", encoding="utf-8") as file_handle:
        file_handle.write(htmlmin.minify(map_soup, remove_empty_space=True))
except Exception as error_msg:
    logging.error(f"minification of '{map_html_file}' failed -> error: {error_msg}")
    with open(map_html_file, "w+", encoding="utf-8") as file_handle:
        file_handle.write(map_soup)

print("done")
