"""

@title TinyWeatherForecastGermany - areas.txt contents - GitLab Pages page

@author Jean-Luc Tibaux (https://gitlab.com/eUgEntOptIc44)

@license GPLv3

@since July 2021

No warranty of any kind provided. Use at your own risk only.
Not meant to be used in commercial or
in general critical/productive environments at all.

"""

import base64
import json

# from pprint import pprint
import logging
from datetime import datetime  # , date, timedelta
from pathlib import Path

import htmlmin
import regex
import requests
from bs4 import BeautifulSoup

# timezone UTC -> docs: https://dateutil.readthedocs.io/en/stable/tz.html#dateutil.tz.tzutc
from dateutil.tz import tzutc

working_dir = Path("public")
working_dir.mkdir(parents=True, exist_ok=True)  # create directory if not exists

log_p = working_dir / "debug.log"
try:
    logging.basicConfig(
        format="%(asctime)-s %(levelname)s [%(name)s]: %(message)s",
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(str(log_p.absolute()), encoding="utf-8"),
            logging.StreamHandler(),
        ],
    )
except Exception as error_msg:
    logging.error(f"while logger init! -> error: {error_msg}")

api_base_url = "https://codeberg.org/api/v1"

api_repo_url = api_base_url + "/repos/Starfish/TinyWeatherForecastGermany"

api_contents_url = api_repo_url + "/contents"


def string_to_base64(string, encoding="utf-8"):
    """
    convert string to base64 encoded string

    based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    """
    return base64.b64encode(string.encode(encoding))


def base64_to_string(base64str, encoding="utf-8"):
    """
    convert base64 encoded string to string

    based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    """
    return base64.b64decode(base64str).decode(encoding)


def json_rest_req(url_temp):
    """
    request to REST api endpoint `url_temp` using `requests`
    """

    url_tmp = str(url_temp).strip()

    headers = {}

    logging.debug(f"preparing json api request for '{url_tmp}'")

    try:
        if len(url_tmp) > 0:
            req_temp = requests.get(url_tmp, headers=headers, timeout=30)

            if req_temp.status_code == 200:
                json_temp = json.loads(req_temp.text)

                if len(str(json_temp)) > 4:
                    if json_temp is None:
                        logging.error(
                            "content of parsed json api request anwser"
                            f" for '{url_tmp}' is invalid!"
                            f" -> content: {json_temp}"
                        )
                        return []

                    return json_temp
                else:
                    logging.error(
                        "length of parsed json api request anwser"
                        f" for '{url_tmp}' is invalid! "
                    )
                    return []
    except Exception as error_msg:
        logging.error(f"json api request for '{url_tmp}' failed! -> error: {error_msg}")
        return []


# data source: https://codeberg.org/Starfish/TinyWeatherForecastGermany/src/branch/master/app/src/main/res/raw/areas.txt
areas_file_json = json_rest_req(api_contents_url + "/app/src/main/res/raw/areas.txt")
# print(areas_file_json)

areas_tmp_json_p = working_dir / "twfg-areas-temp.json"
with open(areas_tmp_json_p, "w+", encoding="utf-8") as file_handle:
    file_handle.write(str(json.dumps(areas_file_json, indent=4)))
# with open(areas_tmp_json_p, "r", encoding="utf-8") as fh:
#    areas_file_json = json.loads(str(fh.read()))

areas_file_content = str(areas_file_json["content"])
areas_file_enc = str(areas_file_json["encoding"]).strip()

if areas_file_enc == "base64":
    areas_file_content = str(base64_to_string(areas_file_json["content"]))
else:
    logging.warning(
        f"detected unexpected file encoding '{areas_file_enc}'"
        f" of 'areas.txt' ({areas_file_json['url']}) "
    )

areas_txt_p = working_dir / "areas.txt"
with open(areas_txt_p, "w+", encoding="utf-8") as file_handle:
    file_handle.write(areas_file_content)
# with open(areas_txt_p, "r", encoding="utf-8") as fh:
#    areas_file_content = str(fh.read())

areas_file_ln = list(areas_file_content.split("\n"))

"""
data format -> split by '@':

    example: 209902000@206@2@"Rothsee"@49.211563,11.18254 49.226807,11.194911 49.23832,11.208389 49.22707,11.188899 49.225174,11.176717 49.211563,11.18254

    0 -> warncellID -> 209902000
    1 -> warncenter -> 206
    2 -> type -> 2
    3 -> name -> Rothsee # quotes have to be removed
    4 -> polygonString
            49.211563
            11.18254 49.226807
            11.194911 49.23832
            11.208389 49.22707
            11.188899 49.225174
            11.176717 49.211563
            11.18254
"""

result_list_parsed_areas = []

for lineIndex in range(len(areas_file_ln)):
    try:
        # line 0 contains the metadata
        # e.g. Geodata version: 3, 24.05.2022
        if lineIndex > 0:
            line_tmp = str(areas_file_ln[lineIndex]).strip()
            if len(line_tmp) < 5:
                logging.warning(
                    f"skipping line {lineIndex + 1} of 'areas.txt' -> length < 5"
                )
                if len(line_tmp) > 0:
                    logging.debug(line_tmp)
                continue
            if "@" in line_tmp:
                ln_tmp_parts = line_tmp.split("@")

                if len(ln_tmp_parts) == 5:
                    result_list_parsed_areas.append(
                        {
                            "warncellID": int(ln_tmp_parts[0]),
                            "warncenter": int(ln_tmp_parts[1]),
                            "type": int(ln_tmp_parts[2]),
                            "name": str(ln_tmp_parts[3]).strip().strip('"').strip("'"),
                            "polygonString": str(ln_tmp_parts[4]).strip(),
                        }
                    )
            else:
                logging.error(
                    f"while parsing line {lineIndex + 1}"
                    " of 'areas.txt' -> error: failed to find '@'"
                )
                logging.debug(line_tmp)
    except Exception as error_msg:
        logging.error(
            f"while parsing line {lineIndex + 1} of 'areas.txt' -> error: {error_msg}"
        )
        logging.error(str(areas_file_ln[lineIndex]))

len_parsed_areas = len(result_list_parsed_areas)

if len_parsed_areas == 0:
    logging.error(
        "while parsing 'areas.txt' -> error: no parsed data"
        " in 'result_list_parsed_areas' -> invalid length"
        f" '{len_parsed_areas}' !"
    )
else:
    logging.debug(f"successfully parsed {len_parsed_areas} areas from 'areas.txt'")

twfg_areas_p = working_dir / "twfg-areas.json"
with open(twfg_areas_p, "w+", encoding="utf-8") as file_handle:
    file_handle.write(json.dumps(result_list_parsed_areas, indent=4))
# with open(twfg_areas_p, "r", encoding="utf-8") as fh:
#    len_parsed_areas = json.loads(str(fh.read()))

# we're using index.html as template
index_file = working_dir / "index.html"
index_file_contents = ""
if index_file.exists():
    if index_file.stat().st_size > 0:
        logging.debug(
            f"found file 'index.html' -> size: {index_file.stat().st_size}"
        )
        with open(index_file, "r", encoding="utf-8") as file_handle:
            index_file_contents = str(file_handle.read())
    else:
        logging.error(
            f"found file 'index.html' but size '{index_file.stat().st_size}'"
            " is invalid! -> this might occur due to missing permissions! "
        )
else:
    logging.warning(
        "could not find the file 'index.html'"
        " -> using minimal hardcoded template instead"
    )

try:
    def get_warncell_id(item_tmp):
        return item_tmp["warncellID"]

    result_list_parsed_areas = sorted(result_list_parsed_areas, key=get_warncell_id)
except Exception as error_msg:
    logging.error(f"sorting of 'result_list_parsed_areas' failed! -> error: {error_msg} ")

areas_html_str = '<div id="areas-table-container"><table id="areas-table">'

areas_html_str += '<thead><tr id="areas-table-headings">'
for areas_head_tmp in result_list_parsed_areas[0].keys():
    areas_head_tmp = str(areas_head_tmp).strip()
    if areas_head_tmp.lower() != "polygonstring":
        slug_tmp = str(regex.sub(r"[^A-z\d]+", "", areas_head_tmp))
        areas_html_str += (
            '<th class="areas-table-heading"'
            f' data-sort="{areas_head_tmp}" data-sort-order="desc" '
            f'id="areas-table-heading-{slug_tmp}">'
            f'{areas_head_tmp}</th>'
        )
areas_html_str += '</tr></thead><tbody class="list">'

for area_index_tmp in range(len(result_list_parsed_areas)):
    area_tmp = result_list_parsed_areas[area_index_tmp]
    areas_html_str += (
        '<tr class="areas-table-row"'
        f' data-id="{area_index_tmp}"'
        f' id="areas-table-row-{area_index_tmp + 1}">'
        f'<td class="warncellID">{area_tmp["warncellID"]}</td>'
        f'<td class="warncenter">{area_tmp["warncenter"]}</td>'
        f'<td class="type">{area_tmp["type"]}</td>'
        f'<td class="name">{area_tmp["name"]}</td></tr>'
    )

areas_html_str += (
    '</tbody></table><ul id="areas-table-pagination" class="pagination"></ul></div>'
)

# parse html to insert elements into 'index.html'
areas_html_soup = BeautifulSoup(areas_html_str, features="html.parser")
# print(areas_html_soup)

if len(str(index_file_contents)) > 0 and "html" in str(index_file_contents):
    # parse html to modify elements
    index_file_soup = BeautifulSoup(index_file_contents, features="html.parser")

    index_file_soup.title.string = (
        "areas.txt | open source android app using"
        " open weather data from Deutscher Wetterdienst (DWD)"
    )

    if len(index_file_soup.select("#repo-latest-release-container")) > 0:
        index_file_soup.select("#repo-latest-release-container")[0].decompose()

    if len(index_file_soup.select("#readme-content-container")) > 0:
        index_file_soup.select("#readme-content-container")[0].decompose()

    if len(index_file_soup.select("#repo-metadata-container")) > 0:
        index_file_soup.select("#repo-metadata-container")[0].insert_after(
            areas_html_soup
        )

    now_utc = datetime.now(tzutc())
    index_file_soup.select("#page-timestamp-last-update")[0].string = str(
        now_utc.strftime("%Y-%m-%d at %H:%M (%Z)")
    )
    index_file_soup.select("#page-timestamp-last-update")[0]["data-timestamp"] = str(
        now_utc.strftime("%Y-%m-%dT%H:%M:000")
    )

    areas_html_str = str(index_file_soup)
else:
    areas_html_str = f"<html><head></head><body>{areas_html_str}<body></html>"

areas_html_file = working_dir / "areas.html"
try:
    with open(areas_html_file, "w+", encoding="utf-8") as file_handle:
        file_handle.write(htmlmin.minify(areas_html_str, remove_empty_space=True))
except Exception as error_msg:
    logging.error(f"minification of '{areas_html_file}' failed -> error: {error_msg}")
    with open(areas_html_file, "w+", encoding="utf-8") as file_handle:
        file_handle.write(areas_html_str)

print("done")
