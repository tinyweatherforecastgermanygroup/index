"""

@title TinyWeatherForecastGermany - stations5.txt contents - GitLab Pages page

@author Jean-Luc Tibaux (https://gitlab.com/eUgEntOptIc44)

@license GPLv3

@since July 2021

No warranty of any kind provided. Use only at your own risk only.
Not meant to be used in commercial or
in general critical/productive environments at all.

"""

import base64
import json

# from pprint import pprint
import logging
import sys
from datetime import datetime  # , date, timedelta
from pathlib import Path

import htmlmin
import regex
import requests
from bs4 import BeautifulSoup

# timezone UTC -> docs: https://dateutil.readthedocs.io/en/stable/tz.html#dateutil.tz.tzutc
from dateutil.tz import tzutc

working_dir = Path("public")
working_dir.mkdir(parents=True, exist_ok=True)  # create directory if not exists

log_p = working_dir / "debug.log"
try:
    logging.basicConfig(
        format="%(asctime)-s %(levelname)s [%(name)s]: %(message)s",
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(
                str(log_p.absolute()), encoding="utf-8"
            ),
            logging.StreamHandler(),
        ],
    )
except Exception as error_msg:
    logging.error(f"while logger init! -> error: {error_msg}")

api_base_url = "https://codeberg.org/api/v1"

api_repo_url = api_base_url + "/repos/Starfish/TinyWeatherForecastGermany"

api_contents_url = api_repo_url + "/contents"


def string_to_base64(string, encoding="utf-8"):
    """
    convert string to base64 encoded string

    based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    """
    return base64.b64encode(string.encode(encoding))


def base64_to_string(base64str, encoding="utf-8"):
    """
    convert base64 encoded string to string

    based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    """
    return base64.b64decode(base64str).decode(encoding)


def json_rest_req(url_tmp):
    """
    request to REST api endpoint `url_temp` using `requests`
    """

    url_tmp = str(url_tmp).strip()

    headers = {}

    logging.debug(f"preparing json api request for '{url_tmp}'")

    try:
        if len(url_tmp) > 0:
            req_tmp = requests.get(url_tmp, headers=headers, timeout=15)

            if req_tmp.status_code == 200:
                json_tmp = json.loads(req_tmp.text)

                if len(str(json_tmp)) > 4:
                    if json_tmp is None:
                        logging.error(
                            f"content of parsed json api request anwser"
                            f" for '{url_tmp}' is invalid!"
                            f" -> content: {json_tmp}"
                        )
                        return []

                    return json_tmp
                else:
                    logging.error(
                        f"length of parsed json api request anwser"
                        f" for '{url_tmp}' is invalid! "
                    )
                    return []
    except Exception as error_msg:
        logging.error(
            f"json api request for '{url_tmp}' failed! -> error: {error_msg}"
        )
        return []


stations_txt = "stations5.txt"

# data source: https://codeberg.org/Starfish/TinyWeatherForecastGermany/src/branch/master/app/src/main/res/raw{stations_txt}
stations_file_json = json_rest_req(
    f"{api_contents_url}/app/src/main/res/raw/{stations_txt}"
)
# print(stations_file_json)

with open(
    str(Path(working_dir / "twfg-stations-temp.json").absolute()),
    "w+",
    encoding="utf-8",
) as file_handle:
    file_handle.write(str(json.dumps(stations_file_json, indent=4)))
# with open(str(Path(workingDir / "twfg-stations-temp.json").absolute()), "r", encoding="utf-8") as fh:
#    stations_file_json = json.loads(str(fh.read()))

if stations_file_json is None:
    logging.error("content of 'stations_file_json' is None -> FATAL ERROR ")
    sys.exit(1)

stations_file_content = str(stations_file_json["content"])
st_file_encode = str(stations_file_json["encoding"]).strip()

if st_file_encode == "base64":
    stations_file_content = str(base64_to_string(stations_file_json["content"]))
else:
    logging.warning(
        f"detected unexpected file encoding '{st_file_encode}'"
        f" of '{stations_txt}' ({stations_file_json['url']}) "
    )

stations_txt_p = working_dir / stations_txt
with open(stations_txt_p, "w+", encoding="utf-8") as file_handle:
    file_handle.write(stations_file_content)
# with open(stations_txt_p, "r", encoding="utf-8") as fh:
#    stationsFileContent = str(fh.read())

stations_file_ln = list(stations_file_content.split("|"))

"""
data format -> split by ';':

    example: 
      0 -> TROMSOE -> name
      1 -> 01025 -> id
      2 -> 18.92,69.68,10.0 -> needs to be split by ',' 
"""

result_list_parsed_stations = []

for ln_index in range(len(stations_file_ln)):
    try:
        if ln_index > -1:
            line_tmp = str(stations_file_ln[ln_index]).strip()
            if len(line_tmp) < 5:
                logging.warning(
                    f"skipping line {ln_index + 1} of '{stations_txt}' -> length < 5 "
                )
                if len(line_tmp) > 0:
                    logging.debug(line_tmp)
                continue
            if ";" in line_tmp:
                line_tmp_parts = line_tmp.split(";")
                # pprint(line_tmp_parts)

                if len(line_tmp_parts) == 4:
                    line_coords = str(line_tmp_parts[-1]).split(",")

                    """
                    station types: -> see getSourceString() in java code

                    0 -> MOS
                    1 -> DMO
                    2 -> SWSMOS
                    3 -> WARNMOS
                    """

                    result_list_parsed_stations.append(
                        {
                            "type": str(line_tmp_parts[0]),
                            "name": str(line_tmp_parts[1]),
                            "id": str(line_tmp_parts[2]),
                            "latitude": float(line_coords[1]),
                            "longitude": float(line_coords[0]),
                            "altitude": float(line_coords[2]),
                        }
                    )
            else:
                logging.error(
                    f"while parsing line {ln_index + 1}"
                    f" of '{stations_txt}' -> error: failed to find '@' "
                )
                logging.debug(line_tmp)
    except Exception as error_msg:
        logging.error(
            f"while parsing line  {ln_index + 1} of '{stations_txt}' -> error: {error_msg}"
        )
        logging.error(str(stations_file_ln[ln_index]))

len_parsed_stations = len(result_list_parsed_stations)

if len_parsed_stations == 0:
    logging.error(
        f"while parsing '{stations_txt}' -> error: no parsed data in"
        f" 'result_list_parsed_stations' -> invalid length '{len_parsed_stations}' !"
    )
    sys.exit(0)
else:
    logging.debug(
        f"successfully parsed {len_parsed_stations} stations from '{stations_txt}'"
    )

twfg_stations_p = working_dir / "twfg-stations.json"
with open(twfg_stations_p, "w+", encoding="utf-8") as file_handle:
    file_handle.write(json.dumps(result_list_parsed_stations, indent=4))
# with open(twfg_stations_p, "r", encoding="utf-8") as fh:
#    lenresult_list_parsed_stations = json.loads(str(fh.read()))

# we're using index.html as template
index_file = working_dir / "index.html"
index_file_contents = ""
if index_file.exists():
    if index_file.stat().st_size > 0:
        logging.debug(f"found file 'index.html' -> size: {index_file.stat().st_size}")
        with open(index_file, "r", encoding="utf-8") as file_handle:
            index_file_contents = str(file_handle.read())
    else:
        logging.error(
            f"found file 'index.html' but size '{index_file.stat().st_size}'"
            " is invalid! -> this might occur due to missing permissions! "
        )
else:
    logging.warning(
        "could not find the file 'index.html' -> "
        "using minimal hardcoded template instead"
    )

try:

    def get_id(item_tmp):
        return str(item_tmp["id"])

    result_list_parsed_stations = sorted(result_list_parsed_stations, key=get_id)
except Exception as error_msg:
    logging.error(f"sorting of 'result_list_parsed_stations' failed! -> error: {error_msg}")

stations_html_str = '<div id="stations-table-container"><table id="stations-table">'

stations_html_str += '<thead><tr id="stations-table-headings">'
for stations_head_tmp in result_list_parsed_stations[0].keys():
    if str(stations_head_tmp).strip().lower() != "none":
        slugTemp = str(regex.sub(r"[^A-z\d]+", "", str(stations_head_tmp).strip()))
        stations_html_str += (
            '<th class="stations-table-heading" '
            'data-sort="' + str(stations_head_tmp).strip() + '"'
            ' data-sort-order="desc" '
            'id="stations-table-heading-'
            + slugTemp
            + '">'
            + str(stations_head_tmp).strip()
            + "</th>"
        )
stations_html_str += '</tr></thead><tbody class="list">'


def get_station_type(type_int):
    try:
        try:
            type_int = int(type_int)
        except TypeError:
            logging.error(
                f"failed to get invalid station type '{type_int}'"
                " -> error: invalid data type "
            )
            return "UNKOWN"
        station_types = ["MOS", "DMO", "SWSMOS", "WARNMOS"]
        if type_int > -1 and type_int < len(station_types):
            return station_types[type_int]
        else:
            return "UNKOWN"
    except Exception as error_msg:
        logging.error(f"failed to get invalid station type '{type_int}' -> error: {error_msg}")
        return "UNKOWN"


for station_index_tmp in range(len(result_list_parsed_stations)):
    station_tmp = result_list_parsed_stations[station_index_tmp]
    stations_html_str += (
        '<tr class="stations-table-row" '
        'data-id="' + str(station_index_tmp) + '" '
        'id="stations-table-row-' + str(station_index_tmp + 1) + '">'
        '<td class="type">' + get_station_type(station_tmp["type"]) + "</td>"
        '<td class="name">' + str(station_tmp["name"]) + "</td>"
        '<td class="id">' + str(station_tmp["id"]) + "</td>"
        '<td class="latitude">' + str(station_tmp["latitude"]) + "</td>"
        '<td class="longitude">' + str(station_tmp["longitude"]) + "</td>"
        '<td class="altitude">' + str(station_tmp["altitude"]) + "</td>"
        "</tr>"
    )

stations_html_str += (
    '</tbody></table><ul id="stations-table-pagination"'
    ' class="pagination"></ul></div>'
)

# parse html to insert elements into 'index.html'
stations_html_soup = BeautifulSoup(stations_html_str, features="html.parser")
# print(stationsHTMLsoup)

if len(str(index_file_contents)) > 0 and "html" in str(index_file_contents):
    # parse html to modify elements
    index_file_soup = BeautifulSoup(index_file_contents, features="html.parser")

    index_file_soup.title.string = (
        f"{stations_txt} | open source android app using"
        " open weather data from Deutscher Wetterdienst (DWD)"
    )

    if len(index_file_soup.select("#repo-latest-release-container")) > 0:
        index_file_soup.select("#repo-latest-release-container")[0].decompose()

    if len(index_file_soup.select("#readme-content-container")) > 0:
        index_file_soup.select("#readme-content-container")[0].decompose()

    if len(index_file_soup.select("#repo-metadata-container")) > 0:
        index_file_soup.select("#repo-metadata-container")[0].insert_after(
            stations_html_soup
        )

    utc_now = datetime.now(tzutc())
    index_file_soup.select("#page-timestamp-last-update")[0].string = str(
        utc_now.strftime("%Y-%m-%d at %H:%M (%Z)")
    )
    index_file_soup.select("#page-timestamp-last-update")[0]["data-timestamp"] = str(
        utc_now.strftime("%Y-%m-%dT%H:%M:000")
    )

    stations_html_str = str(index_file_soup)
else:
    stations_html_str = (
        "<html><head></head><body>" + stations_html_str + "<body></html>"
    )

stations_html_file = Path(working_dir / "stations.html").absolute()

try:
    with open(str(stations_html_file), "w+", encoding="utf-8") as file_handle:
        file_handle.write(htmlmin.minify(stations_html_str, remove_empty_space=True))
except Exception as error_msg:
    logging.error(f"minification of '{stations_html_file}' failed -> error: {error_msg}")
    with open(stations_html_file, "w+", encoding="utf-8") as file_handle:
        file_handle.write(stations_html_str)

print("done")
