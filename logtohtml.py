"""
pretty-print log file to html using Pygments

author: Jean-Luc Tibaux (https://gitlab.com/eUgEntOptIc44)
license: GPLv3
"""
import logging
import sys
from pathlib import Path

import htmlmin  # html minifier
from bs4 import BeautifulSoup
from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers import get_lexer_by_name

working_dir = Path("public")
working_dir.mkdir(parents=True, exist_ok=True)  # create directory if not exists

log_p = working_dir / "debug2.log"
try:
    logging.basicConfig(
        format="%(asctime)-s %(levelname)s [%(name)s]: %(message)s",
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(str(log_p.absolute()), encoding="utf-8"),
            logging.StreamHandler(),
        ],
    )
except Exception as error_msg:
    logging.error(f"while logger init! -> error: {error_msg}")

log_p_2 = working_dir / "debug.log"
try:
    with open(str(log_p_2.absolute()), "r", encoding="utf-8") as fh:
        code = str(fh.read())
except Exception as error_msg:
    logging.error(f"failed to open '{log_p_2}' -> error: {error_msg}")
    sys.exit("FATAL ERROR script execution aborted!")

code_ln = str(code).split("\n")
hl_lines_indices = []

for line_index in range(len(code_ln)):
    # print(codeLines[lineIndex])
    line_tmp = str(code_ln[line_index]).lower()

    if "error:" in line_tmp or " error [" in line_tmp:
        hl_lines_indices.append(line_index + 1)
        logging.debug(f"identified error pattern match in line #{line_index + 1}")

lexer = get_lexer_by_name("logtalk", stripall=True)
logging.debug("lexer init completed")

formatter = HtmlFormatter(
    linenos=True,
    cssclass="sourcecode",
    full=True,
    style="perldoc",
    title="debug.log | Tiny Weather Forecast Germany",
    lineanchors="debuglog",
    lineseparator="<br>",
    hl_lines=hl_lines_indices,
    wrapcode=True,
)
logging.debug("HtmlFormatter init completed")

result = highlight(code, lexer, formatter)
logging.debug("highlight finished")

bs4_lexer = "html.parser"

debug_file_soup = BeautifulSoup(
    str(result), features=bs4_lexer
)  # parse html to modify elements

for meta_tmp in debug_file_soup.select("head > meta"):
    meta_tmp.decompose()

head_html = (
    '\n\n<meta http-equiv="content-type" content="text/html; charset=UTF-8" />\n<meta charset="utf-8">\n'
    '<meta name="viewport" content="width=device-width, initial-scale=1.0">'
    '\n<meta name="robots" content="no-index, no-follow">'
)

head_html += """\n
<link rel="apple-touch-icon" sizes="180x180" href="images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
<link rel="manifest" href="images/site.webmanifest">
<link rel="mask-icon" href="images/safari-pinned-tab.svg" color="#293235">
<link rel="shortcut icon" href="images/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Tiny Weather Forecast Germany">
<meta name="application-name" content="Tiny Weather Forecast Germany">
<meta name="msapplication-TileColor" content="#293235">
<meta name="msapplication-config" content="images/browserconfig.xml">
<meta name="theme-color" content="#293235">
<meta name="description" content="Tiny Weather Forecast Germany - android app using open weather data by DWD">
<meta name="keywords" content="DWD, Deutscher Wetterdienst, android, app, open source, weather, wetter, rainradar, regenradar, map, charts, open data, germany, deutschland, allemagne, duitsland, météo">
<meta name="thumbnail" content="images/icon.png">
"""

debug_file_soup.title.insert_after(
    BeautifulSoup(head_html, features=bs4_lexer)
)  # parse html to modify elements

logging.debug("added additional 'meta' tags")

css_str = """
<style type="text/css">
/*
    dark mode -> added by @eugenoptic44
*/
@media (prefers-color-scheme: dark) {
    body {
        filter: invert(1);
        background: #0c0d17;
    }
}
@media print {
    body {
        filter: invert(0);
        background: transparent;
    }
}
</style>
"""

debug_file_soup.select("head style")[0].insert_after(
    BeautifulSoup(css_str, features=bs4_lexer)
)  # parse html to modify elements

logging.debug("added additional 'css' tag")

debug_html_file = working_dir / "debug.html"
try:
    with open(debug_html_file, "w+", encoding="utf-8") as fh:
        fh.write(
            htmlmin.minify(
                str(debug_file_soup), remove_empty_space=True, remove_comments=True
            )
        )
except Exception as error_msg:
    logging.error(f"minification of '{debug_html_file}' failed -> error: {error_msg}")
    with open(debug_html_file, "w+", encoding="utf-8") as fh:
        fh.write(str(debug_file_soup))

print("done")
