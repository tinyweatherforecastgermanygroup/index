"""

@title TinyWeatherForecastGermany - watchers - GitLab Pages page

@author Jean-Luc Tibaux (https://gitlab.com/eUgEntOptIc44)

@license GPLv3

@since July 2021

No warranty of any kind provided. Use at your own risk only.
Not meant to be used in commercial or in general critical/productive environments at all.

-------------

### TODO

* highlight contributing watchers?

"""

import json
import logging
from datetime import datetime
from pathlib import Path
# from pprint import pprint

import htmlmin
import markdown
import requests
from bs4 import BeautifulSoup
from dateutil.tz import (
    tzutc,  # timezone UTC -> docs: https://dateutil.readthedocs.io/en/stable/tz.html#dateutil.tz.tzutc
)
from markdown.extensions.toc import TocExtension

working_dir = Path("public")
working_dir.mkdir(parents=True, exist_ok=True)  # create directory if not exists

log_p = working_dir / "debug.log"
try:
    logging.basicConfig(
        format="%(asctime)-s %(levelname)s [%(name)s]: %(message)s",
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(
                str(log_p.absolute()), encoding="utf-8"
            ),
            logging.StreamHandler(),
        ],
    )
except Exception as error_msg:
    logging.error(f"while logger init! -> error: {error_msg}")

def json_rest_req(url_temp):
    url_temp = str(url_temp).strip()

    headers = {"DNT": "1"}

    logging.debug(f"preparing json api request for '{url_temp}'")

    try:
        if len(url_temp) > 0:
            req_temp = requests.get(url_temp, headers=headers, timeout=20)

            if req_temp.status_code == 200:
                json_temp = json.loads(req_temp.text)

                if len(str(json_temp)) > 4:
                    if json_temp is None:
                        logging.error(
                            f"content of parsed json api request anwser for '{url_temp}' is invalid!"
                            f" -> content: {json_temp}"
                        )
                        return []

                    return json_temp
                else:
                    logging.error(
                        f"length of parsed json api request anwser for '{url_temp}' is invalid! "
                    )
                    return []
    except Exception as error_msg:
        logging.error(
            f"json api request for '{url_temp}' failed! -> error: {error_msg}"
        )
        return []


watchers_md = "\n# Watchers\n"
watchers_list = []

# ----------------

try:
    codeberg_watchers_json = json_rest_req(
        "https://codeberg.org/api/v1/repos/starfish/TinyWeatherForecastGermany/subscribers"
    )
    # pprint(codeberg_watchers_json)
except Exception as error_msg:
    codeberg_watchers_json = []
    logging.error(f"while trying to fetch codeberg watchers via api -> {error_msg}")

try:
    if len(codeberg_watchers_json) > 0:
        watchers_md += "\n## Codeberg\n"
        for codeberg_watcher in codeberg_watchers_json:
            try:
                watcher_fn = ""
                if len(codeberg_watcher['full_name']) > 0:
                    watcher_fn = f" {codeberg_watcher['full_name']}"

                watchers_md += (f"\n*{watcher_fn} [@{codeberg_watcher['login']}]"
                                f"(https://codeberg.org/{codeberg_watcher['login']}/)")

                watchergazer_dict = {}
                if "login" in codeberg_watcher:
                    watchergazer_dict["login"] = str(codeberg_watcher["login"])
                if "full_name" in codeberg_watcher:
                    watchergazer_dict["full_name"] = str(codeberg_watcher["full_name"])

                if watchergazer_dict != {}:
                    watchergazer_dict[
                        "html_url"
                    ] = f"https://codeberg.org/{codeberg_watcher['login']}/"
                    watchers_list.append(watchergazer_dict)
            except Exception as error_msg:
                logging.error(
                    f"while trying to process codeberg watcher received from api -> {error_msg}"
                )
                logging.error(f"{codeberg_watcher}")
except Exception as error_msg:
    logging.error(f"while trying to process codeberg watchers received from api -> {error_msg}")

# ----------------

try:
    github_watchers_json = json_rest_req(
        "https://api.github.com/repos/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany/watchers"
    )
    # pprint(github_watchers_json)
except Exception as error_msg:
    github_watchers_json = []
    logging.error(f"while trying to fetch github watchers via api -> {error_msg}")

try:
    if len(github_watchers_json) > 0:
        watchers_md += "\n\n## GitHub\n"
        for github_watcher in github_watchers_json:
            try:
                watchers_md += (
                    f"\n* [@{github_watcher['login']}]({github_watcher['html_url']})"
                )

                watchergazer_dict = {}
                if "login" in github_watcher:
                    watchergazer_dict["login"] = str(github_watcher["login"])
                if "html_url" in github_watcher:
                    watchergazer_dict["html_url"] = str(github_watcher["html_url"])

                if watchergazer_dict != {}:
                    watchergazer_dict["full_name"] = ""
                    watchers_list.append(watchergazer_dict)
            except Exception as error_msg:
                logging.error(
                    f"while trying to process github watcher received from api -> {error_msg}"
                )
                logging.error(f"{github_watcher}")
except Exception as error_msg:
    logging.error(f"while trying to process github watchers received from api -> {error_msg}")

# ----------------

try:
    gitea_watchers_json = json_rest_req(
        "https://gitea.com/api/v1/repos/tinyweatherforecastgermanygroup/TinyWeatherForecastGermanyMirror/subscribers"
    )
    # pprint(gitea_watchers_json)
except Exception as error_msg:
    gitea_watchers_json = []
    logging.error(f"while trying to fetch gitea watchers via api -> {error_msg}")

try:
    if len(gitea_watchers_json) > 0:
        watchers_md += "\n\n## Gitea\n"
        for gitea_watcher in gitea_watchers_json:
            try:
                watchers_md += f"\n* {gitea_watcher['full_name']} [@{gitea_watcher['login']}](https://gitea.com/{gitea_watcher['login']}/)"

                watchergazer_dict = {}
                if "full_name" in gitea_watcher:
                    watchergazer_dict["full_name"] = str(gitea_watcher["full_name"])
                if "login" in gitea_watcher:
                    watchergazer_dict["login"] = str(gitea_watcher["login"])

                if watchergazer_dict != {}:
                    watchergazer_dict[
                        "html_url"
                    ] = f"https://gitea.com/{gitea_watcher['login']}/"
                    watchers_list.append(watchergazer_dict)
            except Exception as error_msg:
                logging.error(
                    f"while trying to process gitea watcher received from api -> {error_msg}"
                )
                logging.error(f"{gitea_watcher}")
except Exception as error_msg:
    logging.error(f"while trying to process gitea watchers received from api -> {error_msg}")


with open(
    str(Path(working_dir / "watchers.md").absolute()), "w", encoding="utf-8"
) as file_handle:
    file_handle.write(str(watchers_md))
with open(
    str(Path(working_dir / "watchers.json").absolute()), "w", encoding="utf-8"
) as file_handle:
    file_handle.write(str(json.dumps(watchers_list, indent=4)))

# --------------------------------------------- #

index_file = Path(working_dir / "index.html")  # we're using index.html as template
index_contents = ""
if index_file.exists():
    if index_file.stat().st_size > 0:
        logging.debug(
            f"found file 'index.html' -> size: {index_file.stat().st_size}"
        )
        with open(str(index_file.absolute()), "r", encoding="utf-8") as file_handle:
            index_contents = str(file_handle.read())
    else:
        logging.error(
            f"found file 'index.html' but size '{index_file.stat().st_size}' is invalid!"
            f" -> this might occur due to missing permissions! "
        )
else:
    logging.warning(
        "could not find the file 'index.html' -> using minimal hardcoded template instead"
    )

watchers_md = "\n\n[TOC]\n\n" + watchers_md  # add placeholder for table of contents
watchers_html = markdown.markdown(
    watchers_md,
    extensions=[
        "extra",
        "sane_lists",
        TocExtension(
            baselevel=2, title="Table of contents", anchorlink=True, toc_depth="3-5"
        ),
    ],
)  # 'nl2br',

watchers_html_soup = BeautifulSoup(
    watchers_html, features="html.parser"
)  # parse html to insert elements into 'index.html'
# print(watchers_html_soup)

if len(str(index_contents)) > 0 and "html" in str(index_contents):
    index_file_soup = BeautifulSoup(
        index_contents, features="html.parser"
    )  # parse html to modify elements

    index_file_soup.title.string = ("watchers | open source android app using open "
                                  "weather data from Deutscher Wetterdienst (DWD)")

    if len(index_file_soup.select("#repo-latest-release-container")) > 0:
        index_file_soup.select("#repo-latest-release-container")[0].decompose()

    if len(index_file_soup.select("#readme-content-container")) > 0:
        index_file_soup.select("#readme-content-container")[0].decompose()

    if len(index_file_soup.select("#repo-metadata-container")) > 0:
        index_file_soup.select("#repo-metadata-container")[0].insert_after(
            watchers_html_soup
        )

    utc_now = datetime.now(tzutc())
    index_file_soup.select("#page-timestamp-last-update")[0].string = str(
        utc_now.strftime("%Y-%m-%d at %H:%M (%Z)")
    )
    index_file_soup.select("#page-timestamp-last-update")[0]["data-timestamp"] = str(
        utc_now.strftime("%Y-%m-%dT%H:%M:000")
    )

    watchers_html = str(index_file_soup)
else:
    watchers_html = f"<html><head></head><body>{watchers_html}<body></html>"

watchers_html_file = working_dir / "watchers.html"
try:
    with open(str(watchers_html_file), "w+", encoding="utf-8") as file_handle:
        file_handle.write(htmlmin.minify(watchers_html, remove_empty_space=True))
except Exception as error_msg:
    logging.error(
        f"minification of '{watchers_html_file}' failed -> error: {error_msg}"
    )
    with open(watchers_html_file, "w+", encoding="utf-8") as file_handle:
        file_handle.write(watchers_html)

print("done")
