function aqi_stations() {
    try {
        let folium_map = document.querySelector('.folium-map');
        if (folium_map != undefined) {
            console.log("found folium map", folium_map);
            var aqistations = L.markerClusterGroup();
            const addStationMarker = async (station) => {
                // console.log(station);

                var marker = new L.Marker([station['lat'], station['lon']]);
                marker.addTo(aqistations);

                let station_name = station['name']

                try {
                    if (station['station_url'] != null) {
                        station_name = '<a href="' + station['station_url'] + '" title="Website der Station: ' + station['station_url'] + '" style="color:black;">' + station['name'] + '</a>'
                    }
                } catch (error) {
                    console.log("failed to process 'station_link' of Station '" + station['code'] + "' -> error: " + error)
                }

                marker.bindPopup('<strong>' + station_name + ' (' + station['altitude'] + 'm)</strong><br>' + station['code'] + '<br>' + station['municipality'] + ' <span id="spark-' + station['code'] + '" data-code="' + station['code'] + '"></span>').openPopup();
                marker.bindTooltip(station['name'] + ' (' + station['altitude'] + 'm)').openTooltip();

                marker.on('click', function (event) {
                    try {
                        console.log('trying to load data for AQI station "' + station['code'] + '" ');

                        if (station['operational'] == 1) {
                            fetch('https://dis2datalake.blob.core.windows.net/airquality-derivated/AQI/current/' + station['code'] + '.json', {
                                method: 'GET'
                            })
                                .then(function (response) { return response.json(); })
                                .then(function (json) {
                                    console.log(json);
                                    let spark_data = [];
                                    Object.entries(json).forEach(day => {
                                        if (day[1]['aqi'] != undefined) {
                                            spark_data.push(day[1]['aqi']);
                                        }
                                    });
                                    // console.log(spark_data);

                                    let spark_img = document.createElement('img');
                                    spark_img.style.cssText = 'max-width:98.9%;';

                                    // TODO: replace chart api with chart generation in frontend
                                    // docs: https://quickchart.io/documentation/sparkline-api/
                                    spark_img.setAttribute("src", "https://quickchart.io/chart?c=" + encodeURIComponent("{type:'sparkline',data:{datasets:[{pointRadius: 3, borderWidth: 3, lineTension: 0.3, backgroundColor: getGradientFillHelper('vertical', ['#6287a2', '#e9ecf4']), data:" + JSON.stringify(spark_data) + "}]}}"));
                                    spark_img.setAttribute("loading", "lazy");
                                    document.querySelector('#spark-' + station['code']).append(spark_img);
                                });
                        } else {
                            console.log('ERROR: failed to load for AQI station "' + station['code'] + '" -> error: station is not operational!');
                        }
                    } catch (error) {
                        console.log('ERROR: failed to load for AQI station "' + station['code'] + '" -> error: ' + error);
                        console.log(event);
                    }
                });
            };

            // TODO: fetch AQI values list for marker colours

            fetch('https://tinyweatherforecastgermanygroup.gitlab.io/index/AQI-stations.json', {
                method: 'GET'
            })
                .then(function (response) { return response.json(); })
                .then(function (json) {
                    console.log(json);
                    console.log('received ' + json.length + ' AQI stations ');
                    json.forEach(async (element) => {
                        try {
                            await addStationMarker(element);
                        } catch (error) {
                            console.log("failed to process element of json -> error: " + error);
                        }
                    });

                    if(aqistations == undefined){
                        console.log('failed to add AQI stations to leaflet map -> error: aqistations is undefined!');
                        return false
                    }

                    // console.log('adding ' + aqistations.length + ' AQI stations to leaflet map ... ');
                    try {
                        // defined in script.js
                        console.log(aqistations);
                        twfg_folium_map.addLayer(aqistations);    
                    } catch (error) {
                        console.log('failed to add AQI stations to leaflet map -> error: ' + error);
                    }                    
                });
        }
    } catch (error) {
        console.log("ERROR: failed to execute aqi_stations -> error: " + error);
    }
}

document.addEventListener('DOMContentLoaded', function () {
    console.log("DEBUG: received event 'DOMContentLoaded' ");
    try {
        let map_layer_control = document.querySelector('.leaflet-control-layers-overlays');
        if (map_layer_control != undefined) {
            console.log("DEBUG: creating the 'AQI stations' label ... ");
            let label_container = document.createElement('label');
            let label_div = document.createElement('div');
            label_div.innerHTML = 'AQI stations';
            label_div.setAttribute("title", "External Data: fetched from EEA Azure datalake -> submitted by national weather agencies in Europe.");

            label_container.appendChild(label_div);
            map_layer_control.appendChild(label_container);
            label_container.addEventListener('click', function (event) {
                event.preventDefault();
                console.log(event);
                console.log("DEBUG: received click event for 'AQI stations' ");
                aqi_stations();
            });
        }
    } catch (error) {
        console.log("ERROR: failed to init label for aqi_stations -> error: " + error);
    }
}, false);